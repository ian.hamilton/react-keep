import React, { Component } from 'react';
import ContentEditable from 'react-contenteditable';
import './Note.scss';

class Note extends Component {

    state = {
        note: { key: 0, title: '', text: '' }
    }

    constructor(props) {
        super();
    }

    handleTextChange = (event) => {
        console.log('text event value', event.target.value);
        this.props.onNoteChange(
            {
                ...this.props.note,
                text: event.target.value
            }
        );
    }

    handleTitleChange = (event) => {
        console.log('title event value', event.target.value);
        this.props.onNoteChange(
            {
                ...this.props.note,
                title: event.target.value
            }
        );
    }

    componentDidMount() {
        console.log('props', this.props)
        this.setState({ note: this.props.note });
    }

    handleSaveNote = () => {
        this.props.onSaveNote(this.props.note.noteId);
    }

    handleDeleteNote = () => {
        this.props.onDeleteNote(this.props.note.noteId);
    }

    render() {
        return (

            <React.Fragment>
                <div className="card note shadow">
                    <div className="card-body">
                        <ContentEditable className="card-title bg-secondary text-white"
                            html={this.props.note.title}
                            onBlur={this.handleSaveNote}
                            onChange={this.handleTitleChange}
                        />

                        <ContentEditable className="card-text"
                            html={this.props.note.text}
                            onBlur={this.handleSaveNote}
                            onChange={this.handleTextChange}
                        />

                        <div className="card-footer bg-transparent border-success">
                            <button type="button"
                                className="btn btn-outline-danger"
                                onClick={this.handleDeleteNote}
                            >Delete</button>
                        </div>

                    </div>
                </div>
            </React.Fragment>
        );
    }
}

export default Note;
