import React, { Component } from 'react';
import './Notes.scss';
import Note from './Note';
import axios from 'axios';

class Notes extends Component {

    state = {
        userName: 'me',
        notes: []
    }

    handleAdd = () => {

        const url = 'https://us-central1-pka-forms-fef14.cloudfunctions.net/addNote';
        axios.post(url,
            {
                title: '<b>New Title</b>',
                text: '<b>New Text</b>',
                createdTimestamp: new Date(),
                modifiedTimestamp: new Date(),
                userName: this.state.userName
            }
        ).then(doc => {
            console.log('saved', doc.data);
            this.componentDidMount();
        }
        );
    }

    handleNoteChange = (note) => {
        console.log('handleNoteChange', note);

        const filtered = this.state.notes.filter(n => { return n; });

        let index = 0;
        filtered.find( (n, i) => {
            if(n.noteId === note.noteId) { index = i; }
        })
        
        console.log('index', index);
        console.log('before', filtered);
        filtered[index] = note;   
        console.log('after', filtered);
        this.setState({ ...{}, notes: filtered });
    }

    handleSaveNote = (id) => {

        const url = 'https://us-central1-pka-forms-fef14.cloudfunctions.net/setNote';

        const note = this.state.notes.find(
            note => { return note.noteId === id }
        );

        axios.post(url, note)
            .then(() => {
                console.log('saved');
            }
            )
    }

    handleDeleteNote = (id) => {
        const url = 'https://us-central1-pka-forms-fef14.cloudfunctions.net/deleteNote';

        axios.post(`${url}?noteId=${id}`)
        .then(() => {
            console.log('deleted');
            this.componentDidMount();
        }
        )

    }

    componentDidMount() {
        const url = `https://us-central1-pka-forms-fef14.cloudfunctions.net/getNotes?userName=${this.state.userName}`;

        axios.get(url)
            .then(noteData => {
                console.log('noteData', noteData)
                this.setState({
                    ...this.state,
                    notes: noteData.data
                });
            });
    }

    render() {
        return (
            <div className="notes w-75 p-3 h-75 d-inline-block">

                <button
                    onClick={this.handleAdd}
                    className="btn btn-outline-primary btn-sm m-2">Add Note
                </button>

                <div className="notes-container">

                    {this.state.notes.map(
                        note => (
                            <div className="note-item" key={note.noteId}>
                                <Note key={note.noteId}
                                    note={note}
                                    onSaveNote={this.handleSaveNote}
                                    onDeleteNote={this.handleDeleteNote}
                                    onNoteChange={this.handleNoteChange}
                                />
                            </div>
                        )
                    )}
                </div>
            </div>
        );
    }
}

export default Notes;