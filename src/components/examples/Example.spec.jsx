import React from 'react';
import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import { shallow, mount } from 'enzyme';
import { expect } from 'chai';

import Example from './Example';

Enzyme.configure({ adapter: new Adapter() });

describe('<Example />', () => {

    it('renders <Example /> component with initial state', () => {
        const wrapper = shallow(<Example />);
        expect(wrapper.state().title).to.equal("My Example");
    });

    it('renders <Example /> component with correct title', () => {
        const wrapper = shallow(<Example />);
        expect(
            wrapper.containsMatchingElement(
                <h1>My Example</h1>
            )
        ).to.equal(true);
    });

    it('<Example /> component should add four to numbers', () => {
        const wrapper = mount(<Example />);
        wrapper.find('button').simulate('click');
        expect(
            wrapper.state().numbers.length
        ).to.equal(4);
    });

});