import React, { Component } from 'react'

class MyChild extends Component {

    constructor(props) {
        super();
        console.log('Props Data', props.data);
    }

    handleAddFiveButtonClick = () => {
        this.props.onAdd(this.props.data.value);
    }

    render() {
        return (
            <div>
                <h2>{this.props.data.id}</h2>
                <h3>{this.props.data.value}</h3>
                <button type="button"
                    onClick={this.handleAddButtonClick}
                >Add</button>
            </div>
        );
    }
}