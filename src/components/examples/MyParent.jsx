import React, { Component } from 'react'

class MyParent extends Component {

    state = {
        myData: { id: "abc", value: 123 }
    }

    handleAdd = (currentValue) => {
        const data = this.state.myData;
        data.value = ++currentValue;

        this.setState({
            ...this.state,
            myData: {
                ...{},
                data
            }
        });
    }

    render() {
        return (
            <MyChild
                data={this.state.myData}
                onAdd={this.handleAdd}
            />
        );
    }
}

